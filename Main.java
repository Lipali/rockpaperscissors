import java.util.Random;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        int flagA =0;
        int flagB =0;
        String playerChoice;
        String cpuChoice;
        Scanner scanner = new Scanner(System.in);
        while((flagA<=2)&&(flagB<=2)) {
            System.out.println("rock , paper or scissors ");
            playerChoice = scanner.nextLine();
            cpuChoice= draw() ;
            System.out.println(cpuChoice);

            if (playerChoice.equals(cpuChoice)){
                System.out.println("draw :)");
            }
            else if((playerChoice.equals("paper"))&&(cpuChoice.equals("rock"))) {
                System.out.println("Player win ");
                flagA++ ;}
            else if((playerChoice.equals("paper"))&&(cpuChoice.equals("scissors"))) {
                System.out.println("cpu win  ");
                flagB++ ;}
            else if((playerChoice.equals("rock"))&&(cpuChoice.equals("paper"))) {
                System.out.println("cpu win  ");
                flagB++;}
            else if((playerChoice.equals("rock"))&&(cpuChoice.equals("scissors"))) {
                System.out.println("Player win ");
                flagA++;}
            else if((playerChoice.equals("scissors"))&&(cpuChoice.equals("paper"))) {
                System.out.println("Player win ");
                flagA++;}
            else if((playerChoice.equals("scissors"))&&(cpuChoice.equals("rock"))) {
                System.out.println("cpu win  ");
                flagB++;}

            System.out.println(flagA+"   "+ flagB);
        }
        if (flagA>=2){
            System.out.println("PLAYER WIN GAME !");
        }
        else{
            System.out.println("CPU WIN GAME !");
        }
    }

    static String draw () {
        Random random = new Random();
        int a = random.nextInt(3);

        if (a==0)
            return "rock";
        else if (a==1)
            return "paper";
        else
            return "scissors";
    }
}
